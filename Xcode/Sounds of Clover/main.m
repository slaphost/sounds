//
//  main.m
//  Sounds of Clover
//
//  Created by Jeremy Hicks on 8/24/12.
//  Copyright (c) 2012 Jeremy Hicks. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
